"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var routes_1 = __importDefault(require("./cars/routes"));
var App = /** @class */ (function () {
    function App() {
        this.routers = [];
        this.express = express_1.default();
        //    this.express.use(express.static('dist/webapp'))
        this.routers.push(new routes_1.default());
        this.loadRoutes();
    }
    App.prototype.loadRoutes = function () {
        var _this = this;
        this.routers.forEach(function (router) {
            _this.express.use('/', router.appRoutes());
            _this.express.use('/api/', router.apiRoutes());
        });
    };
    return App;
}());
exports.default = new App().express;
//# sourceMappingURL=App.js.map