"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DynamoItemParser = /** @class */ (function () {
    function DynamoItemParser() {
    }
    DynamoItemParser.prototype.parseJSONItems = function (items) {
        var _this = this;
        return items.map(function (item) {
            var res = {};
            Object.keys(item).forEach(function (key) {
                res[key] = _this.parse(item[key]);
            });
            return res;
        });
    };
    DynamoItemParser.prototype.parse = function (item) {
        var _this = this;
        var ret;
        if (item['M']) {
            ret = {};
            var base_1 = item['M'];
            Object.keys(base_1).forEach(function (key) {
                ret[key] = _this.parse(base_1[key]);
            });
        }
        else if (item['S']) {
            ret = item['S'];
        }
        else if (item['N']) {
            ret = Number(item['N']);
        }
        else if (item['L']) {
            ret = item['L'].map(function (arrayMember) {
                return _this.parse(arrayMember);
            });
        }
        else if (item['BOOL']) {
            ret = item['BOOL'];
        }
        return ret;
    };
    return DynamoItemParser;
}());
exports.default = DynamoItemParser;
//# sourceMappingURL=DynoJSONParser.js.map