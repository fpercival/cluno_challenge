"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var DynoJSONParser_1 = __importDefault(require("../utils/DynoJSONParser"));
var dynamodb_export_json_1 = __importDefault(require("./dynamodb.export.json"));
var CarModel = /** @class */ (function () {
    function CarModel() {
        this.loadData();
    }
    CarModel.prototype.loadData = function () {
        var parser = new DynoJSONParser_1.default();
        this.data = parser.parseJSONItems(dynamodb_export_json_1.default.Items);
    };
    CarModel.prototype.all = function () {
        return this.data;
    };
    CarModel.prototype.getCarById = function (id) {
        return this.data.find(function (dataItem) {
            return dataItem.id === id;
        });
    };
    return CarModel;
}());
exports.default = CarModel;
//# sourceMappingURL=model.js.map