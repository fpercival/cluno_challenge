"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var assert_1 = __importDefault(require("assert"));
var model_1 = __importDefault(require("./model"));
var CarRouter = /** @class */ (function () {
    function CarRouter() {
        this.model = new model_1.default();
    }
    CarRouter.prototype.appRoutes = function () {
        var router = express_1.Router();
        router.get('/cars/:car', this.redirectCars.bind(this));
        return router;
    };
    CarRouter.prototype.apiRoutes = function () {
        var router = express_1.Router();
        router.get('/cars/', this.listVisibleCars.bind(this));
        router.get('/cars/:id', this.getCarById.bind(this));
        return router;
    };
    CarRouter.prototype.listVisibleCars = function (req, res) {
        var carList = this.model.all().filter(function (car) {
            return car.visible === true;
        });
        res.json(carList);
    };
    CarRouter.prototype.getCarById = function (req, res) {
        var car = this.model.getCarById(req.params.id);
        assert_1.default(car);
        res.json(car);
    };
    CarRouter.prototype.redirectCars = function (req, res) {
        res.redirect('/?car=' + req.params.car);
    };
    return CarRouter;
}());
exports.default = CarRouter;
//# sourceMappingURL=routes.js.map