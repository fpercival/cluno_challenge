"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var App_1 = __importDefault(require("./App"));
var express_1 = __importDefault(require("express"));
var port = process.env.PORT || 3000;
App_1.default.use(express_1.default.static('dist/webapp'));
App_1.default.listen(port, function (err) {
    if (err) {
        return console.log(err);
    }
    return console.log("server is listening on " + port);
});
//# sourceMappingURL=index.js.map