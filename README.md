# Cluno coding challenge solution


## Requirements

Node js (developed using v10.17.0)

Typescript - for backend development
```
npm install -g typescript
```

Nodemon - for backend development
```
npm install -g nodemon
```

## Starting

    node dist

Then point browser to

    http://localhost:3000


## Installation

Use script

    ./install.sh

or

using npm

    // in base directory (backend)
    npm install

    // in client directory (frontend)
    npm install


## Building for production

Use script

    ./build.sh

or

using npm

    // in base directory (backend)
    npm run build

    // in client directory (frontend)
    npm run build

Then start with

    node dist


## Development

The frontend was scaffolded and configured using the vue cli utility. This can is installed using

    npm install -g @vue/cli

Start backend in development

    // in base directory
    npm run dev

Start frontend in dev mode in a seperate terminal.

    // in client directory
    npm run serve

Then make your changes.



## Libraries used

### Backend
- Express - Web framework to assist in building web servers
- body-parser - Middleware for express for parsing body content

### Frontend
- axios - Simplify XHR requesting
- quasar - Component library for Vue to assist in design
- vuex - To assist with data loading and binding
- vue-router - Helps mapping url routes to components

All other front end libraries are installed by the @vue/cli utility


