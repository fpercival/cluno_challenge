import DynoJSONParser from '../utils/DynoJSONParser';
import json from './dynamodb.export.json';

class CarModel {

    private data: void[]

    constructor () {
        this.loadData()
    }

    private loadData() {
        const parser: DynoJSONParser = new DynoJSONParser()
        this.data = parser.parseJSONItems( json.Items )
    }

    public all (): void[] {
        return this.data
    }

    public getCarById (id: String): any {
        return this.data.find( (dataItem: any) => {
            return dataItem.id === id
        })
    }
}

export default CarModel
