import { Router, Request, Response } from 'express';
import assert from "assert";
import CarModel from './model'

class CarRouter {

    private model

    constructor () {
        this.model = new CarModel()
    }

    public appRoutes(): Router {
            const router: Router = Router()

            router.get('/cars/:car', this.redirectCars.bind(this) )

            return router
    }

    public apiRoutes(): Router {
            const router: Router = Router()

            router.get('/cars/', this.listVisibleCars.bind(this) )

            router.get('/cars/:id', this.getCarById.bind(this) )

            return router
    }

    private listVisibleCars (req: Request, res: Response): void {
        const carList = this.model.all().filter( car => {
            return car.visible===true
        })
        res.json( carList )
    }

    private getCarById (req: Request, res: Response): void {
        const car = this.model.getCarById(req.params.id)
        assert(car)
        res.json( car )
    }

    private redirectCars (req: Request, res: Response): void {
        res.redirect('/?car='+req.params.car)
    }
}

export default  CarRouter
