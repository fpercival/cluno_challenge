import express from 'express'

import CarRouter from './cars/routes'

class App {
  public express
  private routers: any[] = []

  constructor () {
    this.express = express()

//    this.express.use(express.static('dist/webapp'))
    
    this.routers.push( new CarRouter() )
    this.loadRoutes()
  }

  private loadRoutes (): void {
    this.routers.forEach( router => {
      this.express.use('/', router.appRoutes() )
      this.express.use('/api/', router.apiRoutes() )
    })
  }

}

export default new App().express
