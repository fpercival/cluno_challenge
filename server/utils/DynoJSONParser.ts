/*
    Utility to parse items found inDynamoDB JSON files.
*/

class DynamoItemParser {

    constructor () {
    }

    public parseJSONItems( items: any[] ): any[] {
        return items.map( item => {
            const res = {}
            Object.keys(item).forEach( key => {
                res[key] = this.parse(item[key])
            })
            return res
        })
    }

    public parse(item: any): any {
        let ret
        if (item['M']) {
            ret = {}
            const base = item['M']
            Object.keys(base).forEach( key => {
                ret[key] = this.parse( base[key] )
            })
    
        } else if (item['S']) {
            ret = item['S']
    
        } else if (item['N']) {
            ret = Number(item['N'])
    
        } else if (item['L']) {
            ret = item['L'].map( arrayMember => {
                return this.parse(arrayMember)
            })
    
        } else if (item['BOOL']) {
            ret = item['BOOL']
    
        }
        return ret
    }
}

export default DynamoItemParser
