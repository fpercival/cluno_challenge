import Vue from 'vue'
import App from './App.vue'

import './quasar'

import Vuex from 'vuex'
Vue.use(Vuex)

import vuexStore from './store'
const store = new Vuex.Store(vuexStore)

import router from './routes'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
