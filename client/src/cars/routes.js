import CarList from './components/CarList.vue'
import CarDetails from './components/CarDetails.vue'

export default [
    {path: '/', name: 'carlist', component: CarList},
    {path: '/cars/:id', name: 'carinfo', component: CarDetails}
]