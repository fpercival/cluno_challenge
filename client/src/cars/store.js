import api from './api'

const store = {
    state: {
      cars: undefined,
      selectedCar: undefined,
    },
    getters: {
        getCars: state => state.cars,
        getSelectedCar: state => state.selectedCar
    },
    mutations: {
        setCars: (state, newCars) => state.cars = newCars,
        selectCar: (state, car) => state.selectedCar = car
    },
    actions: {
        async loadCars(context){
            const cars = await api.loadCars()
            cars.sort( (a, b) => {
                return a.pricing.price-b.pricing.price
            })
            context.commit('setCars', cars)
            return cars
        },

        async loadDisplayCar(context, carid){
            let id = carid.split('-').pop()

            const car = await api.loadCarById(id)

            context.commit('selectCar', car)

            return car
        },
    }
}

export default store
