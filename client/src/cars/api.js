import axios from 'axios'

const base =  '/api'

export default {
    async loadCars(){
        const res = await axios.get(`${base}/cars?` + (new Date().getTime() ))
        return res.data
    },
    async loadCarById(id){
        const res = await axios.get(`${base}/cars/${id}`)
        return res.data
    }
}