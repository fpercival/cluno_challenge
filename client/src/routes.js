import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

let routes = []

import carRoutes from './cars/routes'
routes = routes.concat(carRoutes)

export default new VueRouter({
    mode: 'history',
    routes
})