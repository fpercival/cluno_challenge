import carStore from './cars/store'

const store = {
    state: {
        ...carStore.state
    },
    getters: {
        ...carStore.getters
    },
    mutations: {
        ...carStore.mutations
    },
    actions: {
        ...carStore.actions
    }
}

export default store
