/*
    Vue Mixin to simplify this.$store.dispatch by providing the this.$dispatch method.
    Also provides an error property which is set should the vuex action fail.
*/
export default {
    data(){
        return {
            error: undefined,
        }
    },
    methods: {
        async $dispatch(actionName, actionArg, errorMessage){
            this.error = undefined
            try {
                return await this.$store.dispatch(actionName, actionArg)
            } catch(err) {
                this.error = errorMessage || `Es gab ein Fehler bei "${actionName}"`
            }
        }
    },
}